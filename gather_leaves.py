# this routine walks the leave directory and gathers the results into a super structure.

from collections import OrderedDict
from leave_file import LeaveFile
import multiprocessing as multi
import cPickle as pickle
import os, sys

__author__ = 'BNL28'

def gather(directory, outputfile=None):
    """
    :param directory: Directory where all the leave files live (under user names)
    :param outputfile: name for a file in which a pickled copy of the data can be left
    :return: dictionary of leavefile instances
    """
    data = OrderedDict()
    users = os.listdir(directory)
    print 'Gathering using %s processors!', multi.cpu_count()
    pool = multi.Pool(multi.cpu_count())
    for user in users:
        dd = os.path.join(directory, user)
        files = os.listdir(dd)
        filenames = [os.path.join(dd, f) for f in files]
        results = pool.map(LeaveFile, filenames)
        data[user] = results
    if outputfile:
        with open(outputfile, 'w') as ofile:
            pickle.dump(data, ofile)
    return data

def read_gathered(filename):
    """ Read gathered leaves from a pickle file
    :param filename: A pickle of gathered_leaves
    :return:
    """
    with open(filename, 'r') as f:
        data = pickle.load(f)
    ucount = 0
    lcount = 0
    for u in data:
        ucount +=1
        lcount +=len(data[u])
    print 'Read %s leave files from %s users' % (lcount, ucount)

    return data

if __name__ == "__main__":
    if len(sys.argv) == 1:
        dfile = 'data'
    else:
        dfile = sys.argv[1]
    print 'Working on', dfile
    data = gather(dfile, 'gathered_leaves.pickle')



