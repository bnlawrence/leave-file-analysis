# README #

A small set of python scripts for reading and analysing collections of leave files produced by the Unified model.

There are three files currently:

* **leave_file.py** includes the key class which parses and holds the information in a leave file.
* **gather_leaves.py** is used to gather up the information in a directory of such files, summarise them, and write the information out into a pickled structure, which
* **sweep_leaves.py** can be used for plotting.

Usage is pretty straight forward:


```
#!python

python gather_leaves.py your_directory_name
python sweep_leaves.py (possibly_your_pickle_name)

```


### Who do I talk to? ###

* Bryan Lawrence
* Grenville Lister