# provides a class which reads and extracts properties from a leave file
import re, os
import pandas as pd
import traceback

__author__ = 'BNL28'

def string_strip(string, anchor):
    """ Utility method to remove text up to and including anchor from string """
    i = string.find(anchor) + len(anchor)
    return string[i:]

class CleverDict(object):
    """ Used as a mixin for a printable set of attributes behaving a bit like a dictionary  """
    def __init__(self):
        return NotImplemented
    def __str__(self):
        assert hasattr(self,'keys')
        s = ''
        for k in self.keys:
            s += '%s:<%s>; ' % (k, getattr(self, k))
        if s != '':
            return s[0:-2]
        return s


class Coupled(CleverDict):
    """ Holds the informaton about NEMO and CICE for a coupled model"""
    def __init__(self, content):
        bits = content.split()
        print bits
        self.nemo = int(bits[1])
        self.cice = int(bits[3])
        self.keys = ['nemo','cice']


class PBSEpilogue(CleverDict):
    """ Used to parse and understand a PBS epilogue"""
    def __init__(self, content):
        content = content.split('\n')
        for line in content:
            if line.startswith('Total Tasks'):
                self.ncpus = int(string_strip(line,':'))
            elif line.startswith('Elapsed Time'):
                walltime = string_strip(line,':')
                t = map(int, walltime.split(':'))
                s = t[0]*3600+t[1]*60+t[2]
                self.walltime = s
        self.core_hours = self.walltime*self.ncpus/3600.
        self.keys = ['walltime','ncpus','core_hours']


class Resources(CleverDict):
    """ Job resources used """
    def __init__(self, resource_string):
        """ Cray scheduler resources ...
        :param resource_string: The output string from the Cray HPC scheduler epilogues
        """
        self.keys = []
        try:
            bits = resource_string.split(',')
            for b in bits:
                k, v = b.split('=')
                setattr(self, k, v)
                self.keys.append(k)
        except Exception, err:
            err = str(err) + ' [[Unable to read resource string <%s>]]' % resource_string
            print err
            return None
        if 'walltime' in self.keys:
            t = map(int, self.walltime.split(':'))
            s = t[0]*3600+t[1]*60+t[2]
            self.walltime = s
            if 'ncpus' in self.keys:
                n = int(self.ncpus)
                self.keys.append('core_hours')
                self.core_hours = n * s/3600.


class Speedup(CleverDict):
    """ Reads the speedup information"""
    def __init__(self, block_of_lines):
        lines = block_of_lines.split('\n')
        self.cpu_time = float(lines[0])
        self.wallclock = float(string_strip(lines[1],':'))
        self.speedup = float(string_strip(lines[2],':'))
        self.keys = ['cpu_time', 'wallclock', 'speedup']


class MPP(CleverDict):
    """ Reads the timer information"""
    def __init__(self, block_of_lines):
        lines = block_of_lines.split('\n')
        c = lines[1].find('tion')+5
        self.decomposition = map(int, lines[1][c:].split('x'))
        c = lines[2].find(':')+1
        if c != 0:
            self.threads = int(lines[2][c:])
        else:
            self.threads = 1
        self.keys = ['decomposition', 'threads']

class Components(object):
    def __init__(self, block_of_lines):
        # Couldn't work out how to get the second wallclock block using
        # the first member of a group ... so we just pull it out here
        # since we stripped it the firt time
        result = re.search(r'(?<=WALLCLOCK  TIMES$)(.*)', block_of_lines, re.DOTALL|re.M).group()
        lines = result.split('\n')[1:-2]
        # We hope the column numbers don't change ...
        cols = [4, 25, 34, 43, 52, 64, 73, 82, 91, 100]
        splits = [(cols[i], cols[i+1]) for i in range(len(cols)-1)]
        data = []
        for line in lines:
            data.append([line[i:j] for i,j in splits])
        # we don't dataframe this now, because it won't pickle nicely
        self.data = data

    def __str__(self):
        return str(self.data)


class LeaveFile(object):

    def __init__(self, filename, stop_on_error=False):
        """ Instantiate by opening a file, extracting key properties, and then closing the file
        leaving the object holding those properties
        :param filename: The filename of the original leave file
        :return:
        """
        self.data = {'name':os.path.split(filename)[-1]}
        # At some point we need to get the following nicely, but for now they are ignored,
        # we get what we can.

        properties = [
            ('date','datetime','1.1','Date of execution'),
            ('user','str','1.1','username responsible for job'),
            ('jobname','str','1.1','UM job eg. xabcde'),
            ('aprun','str','1.1','aprun command used'),
        #    ('time_simulated','cim::duration','0.1','Duration of run, if run completed'),
            ('decomposition','str','0.1','Decomposition string e.g. 48x24'),
            ('cores_used','int','1.1','Number of cores used'),
            ('nodes_used','int','1.1','Number of nodes used'),
        #    ('compiler_used','str','0.1','Compiler used, if known'),
            ('max_working_set','int','0.1','Maximum memory used'),
            ('um_version','str','0.1','UM Release')
        ]

        self.__get_properties(filename, stop_on_error)

    def __getitem__(self, item):
        """ Extract property of model
        :param item: property of interest
        :return:
        """
        return self.data[item]

    def __setitem__(self, item, value):
        """ No method available for setting internal dictionary properties externally
        """
        return NotImplementedError('No direct set method available')

    def __iter__(self):
        return self.data.__iter__()

    def __get_properties(self, filename, stop_on_error):
        """ parses file for properties"""
        expressions = {
            'um_version': (r'(?<=^     Version)(.*)(?=template)',[], None),
            'date':       (r'(?<=^     Job started at : ).*',[], pd.to_datetime),
            'resources':  (r'(?<=^Resources allocated: ).*$(.*)',[], Resources),
            'aprun':      (r'^aprun.*$',[], None),
            'mpp':        (r'(?<=^MPP Timing information :)(.*)(?=MPP : Non)',[re.DOTALL,], MPP),
            'speedup':    (r'(?<=Total Elapsed CPU Time:)([^-]*)',[re.DOTALL,], Speedup),
            'components': (r'(?<=WALLCLOCK  TIMES)(.*)(?=CPU TIMES)',[re.DOTALL,], Components),
            'pbs':        (r'(?<=PBS epilogue).*$(.*)',[re.DOTALL,],PBSEpilogue),
            'coupled':    (r'^NEMO_NPROC \d.*$',[],Coupled),
        }

        print 'Processing ', filename
        with open(filename, 'r') as f:
            content = f.read()
            missing = []
            for e in expressions:
                try:
                    flags = re.MULTILINE
                    for flag in expressions[e][1]:
                        flags = flags | flag
                    regex = re.compile(expressions[e][0], flags=flags)
                    r = regex.search(content)
                    if r is not None:
                        if expressions[e][2] is not None:
                            r = expressions[e][2](r.group())
                        else:
                            r = r.group()
                        self.data[e] = r
                        if self.data[e] is None:
                            missing.append(e)
                            del self.data[e]
                    else:
                        missing.append(e)
                except Exception, err:
                    estring = '======\nFailed with %s (%s) \n' % (filename, e)
                    estring += traceback.format_exc()
                    estring += '======'
                    print estring
                    if stop_on_error:
                        raise Exception(err)
                    else:
                        erf = os.path.split(filename)[-1]
                        erf = 'error_for.%s' % erf
                        with open(erf,'w') as fe:
                            fe.write(estring)
            # We probably need to get more sophisticated about the timestep parsing,
            # but most of this works whether endgame or not ...
            endgame = True
            if endgame:
                ts = r'^Atm_Step: Timestep.*$|^ Atm_Step: Timestep.*$'
                regex = re.compile(ts,re.MULTILINE)
                strings = regex.findall(content)
                key = 'timesteps'
                if strings:
                    print strings[0], strings[-1]
                    self.data[key] = [len(strings), string_strip(strings[0], 'Model time:'),
                                      string_strip(strings[-1], 'Model time')]
                else:
                    missing.append(key)

            # we hope now that we have either pbs, or resource, info available, we can't have both.
            if 'pbs' in self.data and 'resources' in missing:
                self.data['resources'] = self.data['pbs']
                del self.data['pbs']
                missing.remove('resources')
            elif 'resources' in self.data and 'pbs' in missing:
                missing.remove('pbs')

            if missing:
                if 'um_version' in self.data:
                    version = self.data['um_version']
                else:
                    version = None
                print '%s (v%s) is missing %s' % (filename, version, missing)

        print self

    def __str__(self):
        s = 'LeaveFile: <%s>\n' % self.data['name']
        for x in self.data:
            if x not in ['name','components']: s += '%s: [%s]; ' % (x, self.data[x])
        s = s[0:-1]+'\n'
        return s


if __name__=="__main__":
    for f in [
        'xjrfc000.xjrfc.d14167.t074017.leave',
        'xlrdc000.xlrdc.d15282.t125033.leave.txt',
        'data/grenvill/xjark000.xjark.d14181.t155920.leave',
        'data/grenvill/xjfqu000.xjfqu.d13346.t141644.leave',
        'xkkig000.xkkig.d15104.t102728.leave',
        'xltzm000.xltzm.d15254.t090955.leave',
            ]:
        lf = LeaveFile(f, stop_on_error=True)






