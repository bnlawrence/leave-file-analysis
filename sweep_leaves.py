from gather_leaves import read_gathered
import sys
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
from collections import OrderedDict
import matplotlib.patches as patches

__author__ = 'BNL28'


def make_dataframe(data):
    """
    :param data: gathered_leaves
    :return: dataframe version of key parts of leaves
    """
    output = []
    required = [('um_version', None), ('resources', 'walltime'), ('resources', 'core_hours'),
                ('resources','ncpus'), ]

    def __getstuff(record):
        d = []
        for k,a in required:
            if a is None:
                if k in record:
                    d.append(record[k])
                else:
                    d.append(None)
                #print record['name'], d[-1]
            else:
                if k in record:
                    r = record[k]
                else:
                    r = None
                if r:
                    d.append(getattr(r, a))
                else:
                    d.append(None)
        return d

    def __get_sizes(ch):
        """ Categorise job size based on core hour bins
        bins = [0,100],[100,1000][>1000]
        """
        if ch < 100: return 'small'
        if ch > 100 and ch < 1000: return 'medium'
        if ch > 1000: return 'large'

    def __makeint(v):
        if v is None:
            return None
        else: return int(v)

    count = 0
    for u in data:
        for lf in data[u]:
            output.append(__getstuff(lf))
            if 'um_version' in lf:
                if '7.3' in lf['um_version']:
                    if 'resources' in lf:
                        print lf['resources']
                    else:
                        count+=1
            else:
                print 'No V?'
                for x in lf: print x, lf[x]

    colnames = []
    for k,a in required:
        if a is None:
            colnames.append(k)
        else:
            colnames.append(a)

    dataframe = pd.DataFrame(output,columns=colnames)
    dataframe.convert_objects(convert_numeric=True)

    dataframe['ncpus'] = dataframe['ncpus'].map(__makeint)
    dataframe['sizes'] = dataframe['ncpus'].map(__get_sizes)

    return dataframe


def pie_plot(sums,col,axis,dtype,colors=None):

    """Takes a summed group by , and produces a plot of column name cname,
    with header labels and annotation labels"""
    def __tostring(x):
        if x<1.:
            x = '<1%'
        else:
            x='%3s%%' % int(x)
        return x

    #
    # We can't use groupby plot kind = pie directly because we can't turn the
    # labels off and have a legend ...
    #
    # sums.plot(y='core_hours', kind='pie', ax=ax1, colors=colors, labels=None)
    # plt.legend(loc='left center', fontsize=8, bbox_to_anchor=(-0.1, 1.))
    # ... has no legend unless we remove labels=None, in which case we get the labels
    # ... but then it goes wrong
    col = 'core_hours'
    patches, text = axis.pie(sums[col].values, colors=colors)
    sum = sums[col].sum()
    data = [__tostring(100.*x/sum) for x in sums[col].values]

    labels = ['%6s: %s' % (i.strip(), j) for i,j in zip(sums.index, data)]

    axis.legend(patches, labels, loc='center left', fontsize=8, bbox_to_anchor=(-0.25,0.5))

    ssum = "{:,}".format(int(sum))
    axis.set_title('%s by version\n(%s %s in total)' % (dtype, ssum, dtype))
    axis.set_ylabel('')


def truexlog_histogram(grouped_data, bins, title, colordict, axis,
                       width=0.8, maxgroup=None, tight=False, legend=True):
    """
    :param grouped_data: A pandas grouped by object
    :param bins: A set of bins to use
    :return: a plot
    """
    def __intstr(i):
        if i>10000:
            i=int(i/1000)
            return '%sK' % i
        else:
            return int(i)

    if maxgroup:
        # want the same size bars across all plots, regardless of number of groups
        nbars = maxgroup
    else:
        nbars = len(grouped_data)

    nbins = len(bins) - 1  # the bins are a set of bin edges

    # work out where to plot the various bins
    sub_bars = []
    binedges = []

    for i in range(nbins):
        delta = (np.log10(bins[i+1])-np.log10(bins[i])) * (1.0-width)/2.0
        barset = np.logspace(np.log10(bins[i])+delta, np.log10(bins[i+1]-delta), nbars+1)
        sub_bars.append(barset)
        binedges.append([barset[0], barset[-1]])

    data = OrderedDict()
    i=0
    maxv = None
    for k, gd in grouped_data:
        vals, edges = np.histogram(grouped_data.get_group(k)['core_hours'], bins=bins)
        maxv = max(maxv,max(vals))
        data[k] = [(vals[j], sub_bars[j][i], sub_bars[j][i+1], colordict[k]) for j in range(nbins)]
        i += 1


    axis.set_ylim([0.,maxv])
    for k in data:
        for d in data[k]:
            axis.add_patch(
                # (x,y), width, height
                patches.Rectangle((d[1], 0.0), d[2]-d[1], d[0], facecolor=d[3])
            )

    # Now show the bins
    for i in range(nbins):
        plt.annotate(
            '%s to %s' % (__intstr(bins[i]), __intstr(bins[i+1])),
#            xy=(float(2*i+1)/(2*nbins), 0.), xycoords='axes fraction',
#            xytext=(0, -10), textcoords='offset points', horizontalalignment='center',fontsize=10)
            xy=(float(2*i)/(2*nbins), 0.), xycoords='axes fraction',
            xytext=(0, -10), textcoords='offset points',
            horizontalalignment='left',fontsize=10,rotation=-10)

    axis.set_xscale("log")
    axis.set_xlim([bins[0],bins[-1]])
    axis.xaxis.set_ticklabels([])
    if not tight:
        axis.set_title(title)
        axis.set_xlabel('Size of job (core-hours)')
        axis.set_ylabel('Number of jobs of that size')
    else:
        plt.annotate(title, xy=(0.05, 0.9), xycoords='axes fraction',
                     xytext=(0, 0), textcoords='offset points',
                     horizontalalignment='left', fontsize=10)

    if legend:
        patchboxes = [patches.Patch(color=colordict[v], label=v) for v in data.keys()]
        axis.legend(loc='upper right', bbox_to_anchor=(1., 1.), prop={'size': 8},
                    handles=patchboxes)


def histogram(df, gbv, colours, axis,  title, tight=False, maxgroup=0, legend=True):
    """ Make a plot of distribution of jobs by core-hours """

    MAX = max(df['core_hours'])
    nbins = 5
    binset = 10 ** np.linspace(np.log10(10.0), np.log10(float(MAX)), nbins)

    truexlog_histogram(gbv, binset, title, colours, axis,
                       tight=tight, maxgroup=maxgroup, legend=legend)


def make_version_plots(df, log=False):
    """
    :param df: A dataframe containing the leaves
    :return:
    """

    gbv = df.groupby('um_version')
    print '%s model versions found ' % len(gbv)

    colourset = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black',
                 'lime', 'orange', 'purple', 'gray']

    if len(gbv) > len(colourset):
        print 'Add some html colours from http://www.computerhope.com/htmcolor.htm'
    colourdict = OrderedDict()
    for g, c in zip(sorted(gbv.groups.keys()), colourset[0:len(gbv)-1]):
        colourdict[g] = c

    fig1 = plt.figure()
    p_index = 1
    ax = fig1.add_subplot(2, 2, p_index)

    histogram(df, gbv, colourdict, ax, 'All',
                tight=True, legend=True)
    keys = ['small', 'medium', 'large']
    for k in keys:
        p_index += 1
        dff = df[df['sizes'] == k]
        dg = dff.groupby('um_version')
        ax = fig1.add_subplot(2, 2, p_index)
        histogram(dff, dg, colourdict, ax, '%s' %k.capitalize(),
                  maxgroup=len(gbv), tight=True, legend=False)

    plt.figtext(0.5,0.01,'Size of jobs (in core-hours)',horizontalalignment='center')
    plt.figtext(0.05,0.5,'Number of jobs of this size',horizontalalignment='center',rotation=90)

    plt.show()

    sums = gbv.sum()
    counts = gbv.count()
    print sums, counts

    fig = plt.figure(1)
    ax1 = fig.add_subplot(2, 1, 1, aspect=1.)
    ax2 = fig.add_subplot(2, 1, 2, aspect=1.)
    pie_plot(sums, 'core_hours', ax1, 'core-hours', colors=colourset)
    pie_plot(counts, 'core_hours', ax2, 'jobs', colors=colourset)
    plt.show()

if __name__ == "__main__":

    if len(sys.argv) == 1:
        dfile = 'archer_leaves.pickle'
    else:
        dfile = sys.argv[1]

    print 'Sweeping up ',dfile
    data = read_gathered(dfile)
    dataframe = make_dataframe(data)
    print dataframe
    #make_plots(data)
    make_version_plots(dataframe)

